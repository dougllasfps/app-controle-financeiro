import React from 'react';

interface AppFooterProps {
    layoutColorMode?: string;
}

export const AppFooter: React.FC<AppFooterProps> = (props) => {

    return (
        <div className="layout-footer">
            by
            <span className="font-medium ml-2">Cursos DevBase</span>
        </div>
    );
}
