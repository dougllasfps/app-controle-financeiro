import Axios, { AxiosInstance } from 'axios'

const client: AxiosInstance = Axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
        "Content-type": "application/json"
    },
})

export class ResponseData<S, E> {
    error?: E | undefined;
    data?: S;
}

export interface ValidationError {
    field?: string;
    message?: string;
}

export interface ErrorData {
    message?: string;
    errors?: ValidationError[] | undefined
}

export interface ErrorResponse {
    status?: number;
    data?: ErrorData;
}


export const useHttpClient = () => {

    const post = async <T, E> (url: string, body: T) : Promise<ResponseData<T,E>> => {

        const responseData: ResponseData<T,E> = new ResponseData<T,E>();

        try{
            const response = await client.post<T>(url, body)
            responseData.data = response.data;
        }catch(err: any){
            responseData.error = err.response
        }

        return responseData;
    }

    const put = async <T, E> (url: string, body: T) : Promise<ResponseData<T,E>> => {

        const responseData: ResponseData<T,E> = new ResponseData<T,E>();

        try{
            const response = await client.put<T>(url, body)
            responseData.data = response.data;
        }catch(err: any){
            responseData.error = err.response
        }

        return responseData;
    }

    const get = async <T> (url: string) : Promise<T> => {
        const response = await client.get<T>(url);
        return response.data;
    }

    return {
        post,
        get,
        put
    }
}