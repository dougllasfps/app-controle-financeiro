import { useLocation } from 'react-router-dom'
import query from 'query-string'

export const useQuery = () => {
    const { search } = useLocation();
    return query.parse(search);
}