import React from 'react'

import { Cartao } from 'models/Cartao'
import { FormikHelpers, useFormik } from 'formik'
import { validationScheme } from './validationScheme'
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Conditional } from 'components'
import { ValidationError } from 'config/http/httpClient';


interface CadastroCartoesFormProps {
    cartao: Cartao;
    onSubmit: (cartao: Cartao) => void;
    serverErrors?: ValidationError[];
}

const formInitialValues: Cartao = { name: '' }

export const CadastroCartoesForm: React.FC<CadastroCartoesFormProps> = ({
    cartao,
    onSubmit
}: CadastroCartoesFormProps) => {
 

    const rawOnSubmit = (cartao: Cartao, { setSubmitting, setErrors } : FormikHelpers<Cartao>) => {
        onSubmit(cartao)      
        setTimeout(() => setSubmitting(false), 1500) 
    }

    const { values, errors, handleChange, handleSubmit, isSubmitting } = useFormik<Cartao>({
        initialValues: { ...formInitialValues ,...cartao},
        onSubmit: rawOnSubmit,
        validationSchema: validationScheme,
        validateOnChange: false,
        enableReinitialize: true
    })
    
    return (
        <form onSubmit={handleSubmit}>
            <div className="p-fluid">
                <div className="p-field">
                    <label className="label" htmlFor="id">Código: </label>
                    <InputText 
                            id="id"  
                            placeholder="Seu código irá aparecer aqui quando o registro estiver salvo"
                            name="id" disabled
                            autoComplete="off" 
                            onChange={handleChange}
                            value={values.id} />
                </div>

                <div className="p-field">
                    <label className="label" htmlFor="id">Nome: </label>
                    <InputText 
                            id="name" 
                            name="name"
                            autoComplete="off" 
                            placeholder="Digite o nome do cartão, ex: Visa Pão de Açucar "
                            onChange={handleChange}
                            value={values.name} />
                            <Conditional on={errors.name}>
                                <small className="p-error">{errors.name}</small>
                            </Conditional>
                </div>
                <br />
                <div className="p-col-12">
                    <Button type="submit" 
                            label="Salvar" 
                            loading={isSubmitting} 
                            loadingIcon="pi pi-spin pi-sun">                        
                    </Button>
                </div>
            </div>

        
        </form>
    )
}
