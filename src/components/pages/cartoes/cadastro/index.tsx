import React, { useEffect, useState } from 'react'

import { Cartao } from 'models/Cartao'
import { CadastroCartoesForm } from './Form'
import { useCartoesService } from 'service'
import { ValidationError } from 'yup'
import { useQuery } from 'config/router/useQuery'



export const CadastroCartoesPage = () => {
    const [ cartao, setCartao ] = useState<Cartao>({ id: '', name: '' })
    const [ errors, setErrors ] = useState<ValidationError[]>([])

    const service = useCartoesService();
    const { id } = useQuery()

    const handleSubmit = async (cartao: Cartao) => {
        const { error } = await service.save(cartao)
        if(error?.data?.errors){
            setErrors(error?.data?.errors as ValidationError[])
        }
    }

    const initForm = () => {
        if(id){
            service
                .getById(parseInt(id as string))
                .then(cartao => setCartao(cartao))                
        }
    }

    useEffect(initForm, [id])

    return (
        <div className="col-12">
            <div className="card">
                <h5><i className="pi pi-credit-card"/> {cartao.id ? "Atualização de " : "Novo "} Cartão de Crédito</h5>
                <CadastroCartoesForm serverErrors={errors} cartao={cartao} onSubmit={handleSubmit} />
            </div>
        </div>
    )
}