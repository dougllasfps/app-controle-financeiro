import * as Yup from 'yup'

export const validationScheme = Yup.object().shape({
    name: Yup.string().trim().required("Campo obrigatório").min(3, "Deve ter no mínimo 3 caracteres")
})