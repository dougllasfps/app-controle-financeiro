import React from 'react'

interface ConditionalProps {
    on: any;
    children: React.ReactNode | undefined;
}

export const Conditional: React.FC<ConditionalProps> = ({
    on,
    children
}) => {
    if(on){
        return <>{ children }</>
    }
    return <></>;
}