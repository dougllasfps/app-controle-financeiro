import { ErrorResponse, ResponseData, useHttpClient } from '../config/http/httpClient'
import { Cartao } from 'models/Cartao'

export const useCartoesService = () => {

    const API_URL = '/credit-cards'
    const httpClient = useHttpClient();

    const save = async (cartao: Cartao) : Promise<ResponseData<Cartao, ErrorResponse>> => {
        if(cartao.id){
            return await httpClient.put<Cartao, ErrorResponse>(`${API_URL}/${cartao.id}`, cartao);
        }
        return await httpClient.post<Cartao, ErrorResponse>(API_URL, cartao);        
    }

    const getById = async (id: number) : Promise<Cartao> => {
        return await httpClient.get<Cartao>(`${API_URL}/${id}`);        
    }

    return {
        save,
        getById
    }
}